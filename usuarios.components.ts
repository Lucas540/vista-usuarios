import { Component, OnInit } from '@angular/core';
import { Usuarios } from '../../models/usuarios.models';
import { UsuarioService } from './../../services/services.index';
import { SWAL_ERROR, SWAL_CONFIRMATION } from '../../config/config';
/**
 * DECLARACION A LA FUNCION, QUE SE ENCUENTRA EN CUSTOM.JS
 * ESTA FUNCION ARREGLA DEFECTOS DE IMPLEMENTACION DE LOS
 * PLUGINS Y COMPLEMENTOS DE SIDEBAR, NAVBAR Y TODO LO DEMAS
 * ESTOS ERRORES SE PRESENTAN CUANDO SE PRECIONA EL BOTON
 * DE INGRESAR, AL INICIAR SESION
 *
 */
declare function init_plugins();
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [],
})
export class UsuariosComponent implements OnInit {
  usuarios: Usuarios[] = [];

  offset: number = 0;

  totalRegistros: number = 0;
  cargando: boolean = true;

  constructor(public _USUARIOSERVICES: UsuarioService) { }

  ngOnInit(): void {

    init_plugins();

    this.cargarUsuarios();
  }

  cargarUsuarios() {

    this.cargando = true;
    this._USUARIOSERVICES.read(this.offset).subscribe((resp: any) => {
      console.log(resp);
      this.totalRegistros = resp.DOCUMENTOS;
      this.usuarios = resp.DATOS;
      this.cargando = false;

    });



  }

  cambiarDesde(valor: number) {
    const offset = this.offset + valor;
    console.log(offset);

    if (offset >= this.totalRegistros) {
      return;
    }


    if (offset < 0) {
      return;
    }

    this.offset += valor;
    this.cargarUsuarios();

  }

  buscarUsuario(termino: string) {
    console.log(termino);

    if (termino.length <= 0) {
      this.cargarUsuarios();
      return;
    }
    this.cargando = true;

    this._USUARIOSERVICES.buscarUsuario(termino).subscribe((usuarios: Usuarios[]) => {
      this.usuarios = usuarios;
      this.cargando = false;

    });
  }

  borrarUsusario(usuario: Usuarios) {
    console.log(usuario);
    if (usuario.id_usuario === this._USUARIOSERVICES.usuario.id_usuario) {
      SWAL_ERROR(' No Se Puede Eliminar A Si Mismo', 2200);
      return;
    }

    SWAL_CONFIRMATION('Al', 'Usuario', usuario.nombre).then((resp) => {
      if (resp) {
        this._USUARIOSERVICES.delete(usuario).subscribe((respuesta) => {
          console.log('Usuario Eliminado Correctamente: ' + respuesta);
          this.cargarUsuarios();
        });
      }
    });
  }

  gurdarUsuario(usuario: Usuarios) {
    this._USUARIOSERVICES.update(usuario).subscribe();
  }
}
